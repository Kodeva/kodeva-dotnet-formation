using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlazorServer.Pages
{
    public class SalleAttenteModel : PageModel
    {
        public MyForm Form { get; set; }
        public SalleAttenteModel()
        {

        }
        public void OnGet()
        {
            Form = new MyForm(new List<string>(), "MonTitre");
            Form.Data.Add("JAUNE");
            Form.Data.Add("ROUGE");
        }
    }

    public record MyForm(List<String> Data, String Title);

}
