﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

using IHost host = Host.CreateDefaultBuilder(args).Build();

var conf = host.Services.GetService<IConfiguration>();
var x = conf["Config:Title"];
Console.WriteLine(x);

