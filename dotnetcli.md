## Exercice Créer une solution avec DOTNET CLI - Clean Architecture##
Sample
https://github.com/jasontaylordev/CleanArchitecture/tree/main/src/Application/TodoLists/Commands/CreateTodoList


> La solution doit contenir :
1. un projet WebAPI
2. Un projet Application
2. Un projet Domain
3. Un projet Infrastructure
4. Un projet de test XUnit




## Application Web ##
Liste les Todos
Créer / Modifier / Supprimer un Todo
Créer un Todo Planifié
