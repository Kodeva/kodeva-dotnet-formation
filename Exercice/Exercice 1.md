# Formations

Vérification de la version dotnet installée : 
```c#
dotnet --version
```


## Configuration  : Exercice 1

1. Créer un nouveau projet console en ligne de commande
```powershell
dotnet new app -n 01-Configuration
```
2. Créer un fichier "appsettings.json" à la racine du projet
```json
{
    "Config": {
        "Title" : "Mon titre",
        "Categorie" : [
            {"Key" : "KEY_1","Label" : "Clé 1"},
            {"Key" : "KEY_2","Label" : "Clé 2"}
        ]
    }
}
```
3. Ajouter le package Nugget : **Microsoft.Extensions.Hosting**

4. Dans le fichier Program.cs ajouter les lignes suivantes afin de pouvoir initialiser la configuration
  ```csharp
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using IHost host = Host.CreateDefaultBuilder(args).Build();
```

### Exercice 1.a

> Utilisation du service interne IConfiguration

1. à l'aide de IConfiguration 
    - Afficher dans la console la valeur du Titre 
 ```csharp
 Console.WriteLine($"Mon Titre à la valeur {?}");
```


### Exercice 1.b

> Utilisation du service interne IOptions (Objet / Array)

1. Créer une classe C# reflettant le modèle de l'objet "Config" dans  fichier appsettings.json


### Exercice 1.c

> Utilisation des environnements Development

