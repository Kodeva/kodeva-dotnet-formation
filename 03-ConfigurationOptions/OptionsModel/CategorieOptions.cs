﻿using System.Reflection.Emit;

namespace _03_ConfigurationOptions.OptionsModel
{
    public class CategorieOptions
    {

        public string Libelle { get; set; }

        public string  Couleur { get; set; }
    }
}
