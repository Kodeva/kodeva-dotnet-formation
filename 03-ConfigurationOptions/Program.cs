using _03_ConfigurationOptions.OptionsModel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration
        .AddJsonFile("override.json", optional: true, reloadOnChange: true)
        .AddEnvironmentVariables();

builder.Services.Configure<CategorieOptions>(builder.Configuration.GetSection("CategorieOptions"));

var app = builder.Build();
CategorieOptions opt;
using (var scope = app.Services.CreateScope())
{
     opt = scope.ServiceProvider.GetService<IOptions<CategorieOptions>>().Value;
}
    app.MapGet("/",

        () => $"Hello World! {opt.Couleur}");

app.Run();
