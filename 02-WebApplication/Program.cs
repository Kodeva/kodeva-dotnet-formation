using _02_WebApplication.CustomMiddleware;
using System.Reflection.PortableExecutable;
using System.Xml.Linq;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddMvc(options =>
{

    options.EnableEndpointRouting = false;


});
builder.Services.AddControllers();

var app = builder.Build();

app.UseRouting();

app.MapWhen(d => d.Request.Path == "/exception", app2 =>
{
    app2.UseCustomMiddleware();


});

app.MapWhen(d => d.Request.Path == "/api", async (appBuilder) =>
{
    appBuilder.Run(async (context) => {

        await context.Response.WriteAsync("API");
    });
});

app.MapWhen(d => d.Request.Path == "/www", appBuilder =>
{
    appBuilder.Run(async (context) => {

        await context.Response.WriteAsync("WWW");
    });

});



    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });






app.Run(async context =>
{
    await context.Response.WriteAsync("<p>From middleware 3.</p>");
});

app.Run();
