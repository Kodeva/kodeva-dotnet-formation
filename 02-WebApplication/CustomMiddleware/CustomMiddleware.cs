﻿namespace _02_WebApplication.CustomMiddleware
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
           
            if (context.Request.Query.ContainsKey("name"))
            {
                var name = context.Request.Query["name"];
                await context.Response.WriteAsync($"<p>Bonjour   {name}.</p>");
            }
            await _next(context);
        }
    }

    public static class CustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseCustomMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomMiddleware>();
        }
    }
}
