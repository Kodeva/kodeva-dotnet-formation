## Création d'un background Service ##
> .NET met à disposition une interface IHostedService permettant une implémentation d'un background Service

```csharp 
    Task StartAsync(CancellationToken cancellationToken);
    Task StopAsync(CancellationToken cancellationToken);
```
> **Exercice 1 **  
En Utilisant un Timer  Créer un service permettant d'afficher l'heure dans la console toutes les X secondes
le X secondes devra être paramétable dans le fichier de settings et pouvoir être modifié et pris en compte.
utilisation du **Logger<T>**


```csharp

```
> **Exercice 2 - Corrigé **  
En Utilisant un Cron  Créer un service permettant d'afficher l'heure dans la console toutes les X se condes (réalisé par un cron)
le CRON  devra être paramétable dans le fichier de settings et pouvoir être modifié et pris en compte.
utilisation du **Logger<T>**
> 
Cron Expression : 
```bash
0/10 0 0 ? * * *
```



builder.Service.AddHostedService<T>();

